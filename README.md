## Reservia

### Integration d'une maquette

https://evan34.gitlab.io/evanmartinez_2_08032021

### Fonctionnalités:
* Le champ de recherche est un champ de saisie qui peut être édité par l'utilisateur. 
* Le bouton de recherche ne sera pas fonctionnel.
* Chaque carte d’hébergement ou d’activité devra être cliquable dans son intégralité. Pour l’instant les liens seront vides.
* Les filtres ne seront pas fonctionnels, en revanche, il faut qu’ils changent d’apparence au survol.
* Dans le menu, les liens “Hébergements” et “Activités” sont des ancres qui doivent mener aux sections de la page.

### Contraintes techniques:
* Deux maquettes sont fournies: l'une desktop, l'autre mobile. Le site devra également être adapté aux tablettes.
* Les images sont fournies en différents formats. Il faudra choisir le format le plus adapté par rapport à la résolution et au temps de chargement.
* Les icônes proviennent de la bibliothèque  [Font Awesome](https://fontawesome.com/). Les couleurs de la charte sont le bleu #0065FC, et sa version plus claire #DEEBFF ainsi que le gris pour le fond #F2F2F2.
* La police du site est  [Raleway](https://fonts.google.com/specimen/Raleway).

### Autres:
* Pour ce projet ne pas utiliser de framework, ou de pré-compilateur CSS.
* Le code devra utiliser les balises sémantiques et ne doit contenir aucune erreur ni alerte au validateur W3C HTML et CSS.
* Le site devra être compatible avec les dernières versions de Chrome et Firefox.
* Le code doit être versionné avec Git et déployé sur GitLab pages.


